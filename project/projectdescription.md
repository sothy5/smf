
# Introduction:
Session Management Function (SMF) is a key element in 5G core network for managing data sessions. The detailed description of SMF is provided in [here](https://bitbucket.org/sothy5/smf/src/master/).
In a nutshell, N4 interface is defined for interactions between SMF and User Plane Function (USF), while SMF interacts with other network element using HTTP/2 based protocol.
    
The development of SMF can be either software, hardware, or both combination. The software based SMF gains much flexibility where as hardware based solution acheives great performace improvement.
This project intends to develop SMF using software based techniques, mostly high performance network processing. The most promising approach is using user space TCP/IP backed by Data Plane Development Kit (DPDK) on commidity multicore CPU. In this regard, complete development depends on the [seastar](https://github.com/scylladb/seastar) platform for the above mentioned reason and concurrency programming in C++ using future, promise and continuation.    
      

#Objective:
design and development of complete software stack for SMF without compromsing performance.
The implementation of specified interface (N4, N11, N7 and N10) in C++.
Test with at least one call as specified in TS 23.502 [Section 4.2 or Section 4.3].

#Steps:

 - 5G Network Function (NF) ( for example, SMF is NF) are designed based on service based architecture (SBA). SMF exposed interfaces to AMF is defined in OpenAPI specification. Please see document for SMF-AMF [here](http://www.3gpp.org/ftp/specs/2018-12/Rel-15/OpenAPI/TS29502_Nsmf_PDUSession.yaml)
 - From OpenAPI specification, it is possible to create the server stubs and API client part using OpenAPITools/openapi-generator. However, server stubs in C++ does not work with seastar framework.
 - N4: it is used between SMF and User Plane Function (UPF). This protocol is defined in [3GPP TS 29.244](www.3gpp.org/ftp//Specs/archive/29_series/29.244/29244-f50.zip), evolution of the previous protoocls: [Sxa, Sxb, and Sxc](https://en.wikipedia.org/wiki/PFCP). Initially, we will implement the basic messages such as Association setup, Session Establishment, Session Modification and Session Deletion. [This messages are developed in C](https://bitbucket.org/sothy5/sxcontroller/src/master/sx/), but now we need to develop in C++.        
 

#Reference:
[Shenango: Achieving High CPU Efficiency for Latency-sensitive Datacenter Workloads](https://www.usenix.org/system/files/nsdi19-ousterhout.pdf) 
Amy Ousterhout, Joshua Fried, Jonathan Behrens, Adam Belay, Hari Balakrishnan
NSDI 2019 


  






