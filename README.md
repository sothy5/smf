/*
 * Copyright(c) 2019 Sivasothy SHANMUGALINGAM sothy.e98@gmail.com
 */

# Session Management Function (SMF) 

The 5G architecture proposes SMF for managing PDU sessions in the core network, by combining of control plane entity as in EPC of 4G network.SMF interacts with AMF, V-SMF, PCF, NEF,and PDF using 3GPP defined protocols.

## Terminiology 
   - S-NSSAI: Single network slice selection assistance information. 
   - DNN: Data network Name. 
   - SM: Session Management.
   - PEI: Permanant Equipment Identifier.
   - GUAMI: Globally Unique AMF Identifier.
   - GPSI: Generic Public Subscribtion Identifier.
   - SUPI: Subscription Permanent Identifier.
   - LADN: Local Area Data network.
   - QFI: QoS Flow ID.
   - IMEISV:  


## Architecture

                               
                               
               +---+             +------+     +---------+         +-----+
               |UDM|             |  NEF |     | V-SMF   |         |PCF  |
               |   |             |            |         |         |     |
               |   |             |      |     |         |         |     |
    	       +++++             +-----+-     +---+-----+         +-----+
    		                     |            |                  |
    	     +---------+             |            |                  |
             |         |             |            |                  |
    	     | AMF     +--+          |            |                  |
             |         |  |          |            |                  |
             |         |  |          |            |                  |
             +---------+  |         +-------------+                  |
                          +---------+   SMF       +------------------|   
                                    +----+--------+      
                                         |
                                         |
                                         |                                         
                                         |N4
                                         |
                                   +--------------+
                                   +  	UPF       +
                                   +--------------

## AMF-SMF

This interface has following service and Service operations for PDUSession and EventExposure as listed in Table 5.2.8.1-1 of 3GPP TS 23.502
  
      
   |Service Name 	| Service Operations 	|
   |--------------------|-----------------------|
   |Nsmf_PDUSession     | Create/Update/Release |
   |			| CreateSMContext	|
   |                    | UpdateSMContext       |
   |                    | ReleaseSmContext      |
   |                    | SMContextStatusNotify	|
   |                    | StatusNotify          |
   |	                | Context               |
   |                    |                       |
   |Nsmf_EventExposure  | Subscribe             |
   |                    | Unsubscribe           |
   |                    | Notify                |
   |                    |                       |
   


The complete description of API for PDUSession in the form of OpenAPI format is given [here](http://www.3gpp.org/ftp/specs/2018-12/Rel-15/OpenAPI/TS29502_Nsmf_PDUSession.yaml) by 3GPP.  

For event exposure also, please see [here](http://www.3gpp.org/ftp/specs/2018-12/Rel-15/OpenAPI/TS29508_Nsmf_EventExposure.yaml)  




## SMF-UDM

Nudm_SDM_Unsubscribe
Nudm_UECM_DEregistration

## SMF --pCF
    SM Policy Association Termination

## Adddional Jobs.
    IP address/Prefix.

## Other features of SMF
* UPF Selection Algorithm
* Lawful intercept
* charging and policy related communication
* message communication bus for services Based Interfaces (for event notification, performance data): NGINX, HAProxy, ONAP Micro Service Bus Project(MSB), Apache KafKa [More information](https://mavenir.com/sites/default/files/2019-02/5G-Americas-White-Paper-The-Status-of-Open-Source-for-5G-Feb-2018.pdf)  

## Call Flow
   UE Triggered Service request procedure Fig 4.2.3.2-1 is best use case to test the sample flow.  
   Network Triggered Service Request 
  
   UE Requested PDU Session Establishment (4.3.2.2)
      * Non-roaming and roaming with Local Breakout.
      * Home Routed roaming scenarios.
   
   
## Reference 
   - [SMF PDU Session and Event Exposure APIS](http://github.com/jdegre/5GC_APIs/blob/master/README.md)
   - TS 29.502 for session Management Service
   - TS 29.508 for Session Management Event Exposure
   - TS 33.51X SCAS for 5G (gNB, AMF, SMF, UDM, UPF, AUSF, SEPP, NRF, NEF)
   - [NESAS and SCAS process] (https://docbox.etsi.org/Workshop/2019/201906_ETSISECURITYWEEK/202106_DynamicNatureOfTechno/SESSION01_5GNETWORKS/ERICSSON_NILSSON.pdf)
   - [3GPP Rel 17 study/work Items](https://www.linkedin.com/pulse/rel-17-studywork-items-puneet-jain/)
   - Converged MultiAccess and Core (COMAC), ONF
   - 
    

## Pull request from community.
   if anyone interested to contribute, please feel free to make pull request or contact me. 


















   



