/*
 * Copyright(c) 2019 Sivasothy SHANMUGALINGAM sothy.e98@gmail.com
 */

# Capacity Modeling and Planning for 5GCore 

Traditionaly, Telcom network is over-provisioned in order to manage the bursty traffic. This leads to resource waste around 30-50% at off-peak period. Thanks to virtualization,
resource usage increased ( 40% to 50% => 60%-80%) by automatic provisioning of redundant systems.  

Today, 5GCore is a network function, comprising of multiple containerized microservices. In this context, capacity modeling and planning is tricky.

Queueing theories and steady state modelling is a tool for understanding of capacity of microservices and thus, 5G network function.

Multi-Server, Multi-Phase Queuing Model.      



   
   
## Reference 
   - [5G Capacity lanning - Sizing in a Cloud Native Environment](https://pages.questexweb.com/HPE-Registration-093019.html?source=Umbrella)
   
   - 
    




















   



